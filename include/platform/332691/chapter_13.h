#include <common/register.h>

#ifndef CHAPTER_13_H
#define CHAPTER_13_H

const struct register_t summary_of_i2c_pci_configuration_registers[18];
const struct register_t summary_of_i2c_memory_mapped_registers[37];
const struct register_t summary_of_i2c_additional_registers[16];
const struct register_t summary_of_i2c_dma_controller_registers[41];
const struct register_t summary_of_i2c_pcr_registers[4];

#endif
