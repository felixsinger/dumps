#include <common/register.h>

#ifndef CHAPTER_21_H
#define CHAPTER_21_H

const struct register_t summary_of_ish_pci_configuration_registers[19];
const struct register_t summary_of_ish_mmio_registers[8];

#endif
