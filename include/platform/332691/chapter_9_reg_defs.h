#ifndef CHAPTER_9_REG_DEFS_H
#define CHAPTER_9_REG_DEFS_H

#define REG_9_1_VID	0x0
#define REG_9_1_CMD	0x4
#define REG_9_1_RID	0x8
#define REG_9_1_HT	0xc
#define REG_9_1_MTBLBAR	0x10
#define REG_9_1_MTBUBAR	0x14
#define REG_9_1_SWLBAR	0x18
#define REG_9_1_SWUBAR	0x1c
#define REG_9_1_RTITLBAR	0x20
#define REG_9_1_RTITUBAR	0x24
#define REG_9_1_CAP	0x34
#define REG_9_1_INTL	0x3c
#define REG_9_1_MSICID	0x40
#define REG_9_1_MSILMA	0x44
#define REG_9_1_MSIUMA	0x48
#define REG_9_1_MSIMD	0x4c
#define REG_9_1_NPKDSC	0x80

#endif
