#ifndef CHAPTER_20_REG_DEFS_H
#define CHAPTER_20_REG_DEFS_H

#define REG_20_1_VID	0x0
#define REG_20_1_DID	0x2
#define REG_20_1_CMD	0x4
#define REG_20_1_STS	0x6
#define REG_20_1_RID	0x8
#define REG_20_1_PI	0x9
#define REG_20_1_SCC	0xa
#define REG_20_1_BCC	0xb
#define REG_20_1_CLS	0xc
#define REG_20_1_LT	0xd
#define REG_20_1_HTYPE	0xe
#define REG_20_1_TBAR	0x10
#define REG_20_1_TBARH	0x14
#define REG_20_1_SVID	0x2c
#define REG_20_1_SID	0x2e
#define REG_20_1_CAPPTR	0x34
#define REG_20_1_INTLN	0x3c
#define REG_20_1_INTPN	0x3d
#define REG_20_1_TBARB	0x40
#define REG_20_1_TBARBH	0x44
#define REG_20_1_CB	0x48
#define REG_20_1_PID	0x50
#define REG_20_1_PC	0x52
#define REG_20_1_PCS	0x54
#define REG_20_1_MID	0x80
#define REG_20_1_MC	0x82
#define REG_20_1_MA	0x84
#define REG_20_1_MD	0x88

#define REG_20_2_TEMP	0x0
#define REG_20_2_TSC	0x4
#define REG_20_2_TSS	0x6
#define REG_20_2_TSEL	0x8
#define REG_20_2_TSREL	0xa
#define REG_20_2_TSMIC	0xc
#define REG_20_2_CTT	0x10
#define REG_20_2_TAHV	0x14
#define REG_20_2_TALV	0x18
#define REG_20_2_TSPM	0x1c
#define REG_20_2_TL	0x40
#define REG_20_2_TL2	0x50
#define REG_20_2_PHL	0x60
#define REG_20_2_PHLC	0x62
#define REG_20_2_TAS	0x80
#define REG_20_2_TSPIEN	0x82
#define REG_20_2_TSGPEN	0x84
#define REG_20_2_TCFD	0xf0

#endif
