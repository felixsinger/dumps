#include <common/register.h>

#ifndef CHAPTER_28_H
#define CHAPTER_28_H

const struct register_t summary_of_interrupt_registers[16];
const struct register_t summary_of_interrupt_pcr_registers[20];

#endif
