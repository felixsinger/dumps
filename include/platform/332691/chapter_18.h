#include <common/register.h>

#ifndef CHAPTER_18_H
#define CHAPTER_18_H

const struct register_t summary_of_xhci_configuration_registers[38];
const struct register_t summary_of_xhci_memory_mapped_registers[87];

#endif
