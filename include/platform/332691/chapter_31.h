#include <common/register.h>

#ifndef CHAPTER_31_H
#define CHAPTER_31_H

const struct register_t summary_of_dmi_pcr_registers[57];
const struct register_t summary_of_dci_pcr_registers[1];
const struct register_t summary_of_psf1_pcr_registers[43];
const struct register_t summary_of_psf2_pcr_registers[9];
const struct register_t summary_of_psf3_pcr_registers[49];
const struct register_t summary_of_psf4_pcr_registers[3];
const struct register_t summary_of_io_trap_registers[12];

#endif
