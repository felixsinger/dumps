#ifndef CHAPTER_18_REG_DEFS_H
#define CHAPTER_18_REG_DEFS_H

#define REG_18_1_VID	0x0
#define REG_18_1_DID	0x2
#define REG_18_1_CMD	0x4
#define REG_18_1_STS	0x6
#define REG_18_1_RID	0x8
#define REG_18_1_PI	0x9
#define REG_18_1_SCC	0xa
#define REG_18_1_BCC	0xb
#define REG_18_1_MLT	0xd
#define REG_18_1_HT	0xe
#define REG_18_1_MBAR	0x10
#define REG_18_1_SSVID	0x2c
#define REG_18_1_SSID	0x2e
#define REG_18_1_CAPPTR	0x34
#define REG_18_1_ILINE	0x3c
#define REG_18_1_IPIN	0x3d
#define REG_18_1_XHCC1	0x40
#define REG_18_1_XHCC2	0x44
#define REG_18_1_XHCLKGTEN	0x50
#define REG_18_1_AUDSYNC	0x58
#define REG_18_1_SBRN	0x60
#define REG_18_1_FLADJ	0x61
#define REG_18_1_BESL	0x62
#define REG_18_1_PMCID	0x70
#define REG_18_1_PMNEXT	0x71
#define REG_18_1_PMCAP	0x72
#define REG_18_1_PMCS	0x74
#define REG_18_1_MSICID	0x80
#define REG_18_1_MSINEXT	0x81
#define REG_18_1_MSIMCTL	0x82
#define REG_18_1_MSIMAD	0x84
#define REG_18_1_MSIMUAD	0x88
#define REG_18_1_MSIMD	0x8c
#define REG_18_1_VSHDR	0x94
#define REG_18_1_PCEREG	0xa2
#define REG_18_1_HSCFG2	0xa4
#define REG_18_1_U2OCM	0xb0
#define REG_18_1_U3OCM	0xd0

#define REG_18_2_CAPLENGTH	0x0
#define REG_18_2_HCIVERSION	0x2
#define REG_18_2_HCSPARAMS1	0x4
#define REG_18_2_HCSPARAMS2	0x8
#define REG_18_2_HCSPARAMS3	0xc
#define REG_18_2_HCCPARAMS	0x10
#define REG_18_2_DBOFF	0x14
#define REG_18_2_RTSOFF	0x18
#define REG_18_2_USBCMD	0x80
#define REG_18_2_USBSTS	0x84
#define REG_18_2_PAGESIZE	0x88
#define REG_18_2_DNCTRL	0x94
#define REG_18_2_CRCRLO	0x98
#define REG_18_2_CRCRHI	0x9c
#define REG_18_2_DCBAAPLO	0xb0
#define REG_18_2_DCBAAPHI	0xb4
#define REG_18_2_PORTSCN	0x480
#define REG_18_2_PORTPMSCN	0x484
#define REG_18_2_PORTHLPMCN	0x48c
#define REG_18_2_PORTSCXUSB3	0x580
#define REG_18_2_PORTPMSCX	0x584
#define REG_18_2_PORTLIX	0x588
#define REG_18_2_MFINDEX	0x2000
#define REG_18_2_IMANX	0x2020
#define REG_18_2_IMODX	0x2024
#define REG_18_2_ERSTSZX	0x2028
#define REG_18_2_ERSTBALOX	0x2030
#define REG_18_2_ERSTBAHIX	0x2034
#define REG_18_2_ERDPLOX	0x2038
#define REG_18_2_ERDPHIX	0x203c
#define REG_18_2_DBX	0x3000
#define REG_18_2_XECPSUPPUSB20	0x8000
#define REG_18_2_XECPSUPPUSB21	0x8004
#define REG_18_2_XECPSUPPUSB22	0x8008
#define REG_18_2_XECPSUPPUSB23	0x8010
#define REG_18_2_XECPSUPPUSB24	0x8014
#define REG_18_2_XECPSUPPUSB25	0x8018
#define REG_18_2_XECPSUPPUSB30	0x8020
#define REG_18_2_XECPSUPPUSB31	0x8024
#define REG_18_2_XECPSUPPUSB32	0x8028
#define REG_18_2_XECPSUPPUSB33	0x8030
#define REG_18_2_XECPSUPPUSB34	0x8034
#define REG_18_2_XECPSUPPUSB35	0x8038
#define REG_18_2_XECPSUPPUSB36	0x803c
#define REG_18_2_XECPSUPPUSB37	0x8040
#define REG_18_2_XECPSUPPUSB38	0x8044
#define REG_18_2_XECPSUPPUSB39	0x8048
#define REG_18_2_HOSTCTRLSCHREG	0x8094
#define REG_18_2_PMCTRLREG	0x80a4
#define REG_18_2_HOSTCTRLMISCREG	0x80b0
#define REG_18_2_HOSTCTRLMISCREG2	0x80b4
#define REG_18_2_SSPEREG	0x80b8
#define REG_18_2_DUALROLECFGREG0	0x80d8
#define REG_18_2_AUXCTRLREG1	0x80e0
#define REG_18_2_HOSTCTRLPORTLINKREG	0x80ec
#define REG_18_2_USB2LINKMGRCTRLREG1	0x80f0
#define REG_18_2_USB2LINKMGRCTRLREG4	0x80fc
#define REG_18_2_PWRSCHEDCTRL0	0x8140
#define REG_18_2_PWRSCHEDCTRL2	0x8144
#define REG_18_2_AUXCTRLREG2	0x8154
#define REG_18_2_USB2PHYPMC	0x8164
#define REG_18_2_XHCIAUXCCR	0x816c
#define REG_18_2_XLTPLTV1	0x8174
#define REG_18_2_XLTPHITC	0x817c
#define REG_18_2_XLTPMITC	0x8180
#define REG_18_2_XLTPLITC	0x8184
#define REG_18_2_XECPCMDMCTRLREG2	0x8190
#define REG_18_2_LFPSONCOUNTREG	0x81b8
#define REG_18_2_USB2PMCTRLREG	0x81c4
#define REG_18_2_STRAP2REG	0x8420
#define REG_18_2_USBLEGSUP	0x846c
#define REG_18_2_PDOCAPABILITY	0x84f4
#define REG_18_2_USB2PDO	0x84f8
#define REG_18_2_USB3PDO	0x84fc
#define REG_18_2_DCID	0x8700
#define REG_18_2_SSICPROFILECAPABILITYIDREG	0x8900
#define REG_18_2_PORT1REGISTERACCESSCONTROL	0x8904
#define REG_18_2_PORT1REGISTERACCESSSTATUS	0x8908
#define REG_18_2_PORT1PROFILEATTRIBUTESREG0	0x890c
#define REG_18_2_PORT2REGISTERACCESSCONTROL	0x8a14
#define REG_18_2_PORT2REGISTERACCESSSTATUS	0x8a18
#define REG_18_2_PORT2PROFILEATTRIBUTESREG0	0x8a1c
#define REG_18_2_GLOBALTIMESYNCCAPREG	0x8e10
#define REG_18_2_GLOBALTIMESYNCCTRLREG	0x8e14
#define REG_18_2_MICROFRAMETIMEREG	0x8e18
#define REG_18_2_ALWAYSRUNNINGTIMELOW	0x8e20
#define REG_18_2_ALWAYSRUNNINGTIMEHIGH	0x8e24

#endif
