#ifndef CHAPTER_6_REG_DEFS_H
#define CHAPTER_6_REG_DEFS_H

#define REG_6_1_VID	0x0
#define REG_6_1_DID	0x2
#define REG_6_1_CMD	0x4
#define REG_6_1_DS	0x6
#define REG_6_1_RID	0x8
#define REG_6_1_PI	0x9
#define REG_6_1_SCC	0xa
#define REG_6_1_BCC	0xb
#define REG_6_1_SMBMBAR310	0x10
#define REG_6_1_SMBMBAR6332	0x14
#define REG_6_1_SBA	0x20
#define REG_6_1_SVID	0x2c
#define REG_6_1_SID	0x2e
#define REG_6_1_INTLN	0x3c
#define REG_6_1_INTPN	0x3d
#define REG_6_1_HCFG	0x40
#define REG_6_1_TCOBASE	0x50
#define REG_6_1_TCOCTL	0x54
#define REG_6_1_HTIM	0x64
#define REG_6_1_SMBSM	0x80

#define REG_6_2_HSTS	0x0
#define REG_6_2_HCTL	0x2
#define REG_6_2_HCMD	0x3
#define REG_6_2_TSA	0x4
#define REG_6_2_HD0	0x5
#define REG_6_2_HD1	0x6
#define REG_6_2_HBD	0x7
#define REG_6_2_PEC	0x8
#define REG_6_2_RSA	0x9
#define REG_6_2_SD	0xa
#define REG_6_2_AUXS	0xc
#define REG_6_2_AUXC	0xd
#define REG_6_2_SMLC	0xe
#define REG_6_2_SMBC	0xf
#define REG_6_2_SSTS	0x10
#define REG_6_2_SCMD	0x11
#define REG_6_2_NDA	0x14
#define REG_6_2_NDLB	0x16
#define REG_6_2_NDHB	0x17

#define REG_6_3_TCOCFG	0x0
#define REG_6_3_GC	0xc
#define REG_6_3_PCE	0x10

#endif
