#include <common/register.h>

#ifndef CHAPTER_19_H
#define CHAPTER_19_H

const struct register_t summary_of_usb_device_controller_xdci_configuration_registers[23];
const struct register_t summary_of_xdci_mmio_device_registers[11];
const struct register_t summary_of_xdci_mmio_global_registers[30];

#endif
