#include <common/register.h>

#ifndef CHAPTER_11_H
#define CHAPTER_11_H

const struct register_t summary_of_generic_spi_pci_configuration_registers[18];
const struct register_t summary_of_generic_spi_gspi_memory_mapped_registers[7];
const struct register_t summary_of_generic_spi_gspi_additional_registers[15];
const struct register_t summary_of_generic_spi_gspi_dma_controller_registers[41];
const struct register_t summary_of_gspi_pcr_registers[2];

#endif
