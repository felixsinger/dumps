#include <common/register.h>

#ifndef CHAPTER_5_H
#define CHAPTER_5_H

const struct register_t summary_of_intel_high_definition_audio_d31_f3_pci_configuration_registers[50];
const struct register_t summary_of_intel_high_definition_audio_d31_f3_memory_mapped_i_o_registers[343];

#endif
