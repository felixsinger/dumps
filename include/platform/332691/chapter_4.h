#include <common/register.h>

#ifndef CHAPTER_4_H
#define CHAPTER_4_H

const struct register_t summary_of_power_management_configuration_registers[13];
const struct register_t summary_of_pmc_i_o_based_registers[17];
const struct register_t summary_of_pmc_memory_mapped_registers[68];

#endif
