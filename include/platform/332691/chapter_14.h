#include <common/register.h>

#ifndef CHAPTER_14_H
#define CHAPTER_14_H

const struct register_t summary_of_sata_configuration_registers[38];
const struct register_t summary_of_sata_abar_registers[143];
const struct register_t summary_of_sata_aidp_registers[2];
const struct register_t summary_of_sata_mxpba_registers[1];
const struct register_t summary_of_sata_mxtba_registers[4];
const struct register_t summary_of_sata_sir_index_registers[3];

#endif
