#ifndef CHAPTER_5_REG_DEFS_H
#define CHAPTER_5_REG_DEFS_H

#define REG_5_1_VID	0x0
#define REG_5_1_DID	0x2
#define REG_5_1_CMD	0x4
#define REG_5_1_STS	0x6
#define REG_5_1_RID	0x8
#define REG_5_1_PI	0x9
#define REG_5_1_SCC	0xa
#define REG_5_1_BCC	0xb
#define REG_5_1_CLS	0xc
#define REG_5_1_LT	0xd
#define REG_5_1_HTYPE	0xe
#define REG_5_1_HDALBA	0x10
#define REG_5_1_HDAUBA	0x14
#define REG_5_1_SPCLBA	0x18
#define REG_5_1_SPCUBA	0x1c
#define REG_5_1_ADSPLBA	0x20
#define REG_5_1_ADSPUBA	0x24
#define REG_5_1_SVID	0x2c
#define REG_5_1_SID	0x2e
#define REG_5_1_CAPPTR	0x34
#define REG_5_1_INTLN	0x3c
#define REG_5_1_INTPN	0x3d
#define REG_5_1_PGCTL	0x44
#define REG_5_1_CGCTL	0x48
#define REG_5_1_PID	0x50
#define REG_5_1_PC	0x52
#define REG_5_1_PCS	0x54
#define REG_5_1_MID	0x60
#define REG_5_1_MMC	0x62
#define REG_5_1_MMLA	0x64
#define REG_5_1_MMUA	0x68
#define REG_5_1_MMD	0x6c
#define REG_5_1_PXID	0x70
#define REG_5_1_PXC	0x72
#define REG_5_1_DEVCAP	0x74
#define REG_5_1_DEVC	0x78
#define REG_5_1_DEVS	0x7a
#define REG_5_1_SEM3L	0xc8
#define REG_5_1_SEM4L	0xd0
#define REG_5_1_VCCAP	0x100
#define REG_5_1_PVCCAP1	0x104
#define REG_5_1_PVCCAP2	0x108
#define REG_5_1_PVCCTL	0x10c
#define REG_5_1_PVCSTS	0x10e
#define REG_5_1_VC0CAP	0x110
#define REG_5_1_VC0CTL	0x114
#define REG_5_1_VC0STS	0x11a
#define REG_5_1_VCICAP	0x11c
#define REG_5_1_VCICTL	0x120
#define REG_5_1_VCISTS	0x126

#define REG_5_2_GCAP	0x0
#define REG_5_2_VMIN	0x2
#define REG_5_2_VMAJ	0x3
#define REG_5_2_OUTPAY	0x4
#define REG_5_2_INPAY	0x6
#define REG_5_2_GCTL	0x8
#define REG_5_2_WAKEEN	0xc
#define REG_5_2_WAKESTS	0xe
#define REG_5_2_GSTS	0x10
#define REG_5_2_GCAP2	0x12
#define REG_5_2_LLCH	0x14
#define REG_5_2_OUTSTRMPAY	0x18
#define REG_5_2_INSTRMPAY	0x1a
#define REG_5_2_INTCTL	0x20
#define REG_5_2_INTSTS	0x24
#define REG_5_2_WALCLK	0x30
#define REG_5_2_SSYNC	0x38
#define REG_5_2_CORBLBASE	0x40
#define REG_5_2_CORBUBASE	0x44
#define REG_5_2_CORBWP	0x48
#define REG_5_2_CORBRP	0x4a
#define REG_5_2_CORBCTL	0x4c
#define REG_5_2_CORBSTS	0x4d
#define REG_5_2_CORBSIZE	0x4e
#define REG_5_2_RIRBLBASE	0x50
#define REG_5_2_RIRBUBASE	0x54
#define REG_5_2_RIRBWP	0x58
#define REG_5_2_RINTCNT	0x5a
#define REG_5_2_RIRBCTL	0x5c
#define REG_5_2_RIRBSTS	0x5d
#define REG_5_2_RIRBSIZE	0x5e
#define REG_5_2_IC	0x60
#define REG_5_2_IR	0x64
#define REG_5_2_DPLBASE	0x70
#define REG_5_2_DPUBASE	0x74
#define REG_5_2_ISD0CTL	0x80
#define REG_5_2_ISD0STS	0x83
#define REG_5_2_ISD0LPIB	0x84
#define REG_5_2_ISD0CBL	0x88
#define REG_5_2_ISD0LVI	0x8c
#define REG_5_2_ISD0FIFOW	0x8e
#define REG_5_2_ISD0FIFOS	0x90
#define REG_5_2_ISD0FMT	0x92
#define REG_5_2_ISD0FIFOL	0x94
#define REG_5_2_ISD0BDLPLBA	0x98
#define REG_5_2_ISD0BDLPUBA	0x9c
#define REG_5_2_OSD0LPIB	0x164
#define REG_5_2_OSD0CBL	0x168
#define REG_5_2_OSD0LVI	0x16c
#define REG_5_2_OSD0FIFOW	0x16e
#define REG_5_2_OSD0FIFOS	0x170
#define REG_5_2_OSD0FMT	0x172
#define REG_5_2_OSD0FIFOL	0x174
#define REG_5_2_OSD0BDLPLBA	0x178
#define REG_5_2_OSD0BDLPUBA	0x17c
#define REG_5_2_GTSCC2	0x4dc
#define REG_5_2_WALFCC2	0x4e0
#define REG_5_2_TSCCL2	0x4e4
#define REG_5_2_TSCCU2	0x4e8
#define REG_5_2_LLPFOC2	0x4ec
#define REG_5_2_LLPCL2	0x4f0
#define REG_5_2_LLPCU2	0x4f4
#define REG_5_2_GTSCH	0x500
#define REG_5_2_GTSCD	0x504
#define REG_5_2_GTSCTLAC	0x50c
#define REG_5_2_GTSCC0	0x520
#define REG_5_2_WALFCC0	0x524
#define REG_5_2_TSCCL0	0x528
#define REG_5_2_TSCCU0	0x52c
#define REG_5_2_LLPFOC0	0x534
#define REG_5_2_LLPCL0	0x538
#define REG_5_2_LLPCU0	0x53c
#define REG_5_2_GTSCC1	0x540
#define REG_5_2_WALFCC1	0x544
#define REG_5_2_TSCCL1	0x548
#define REG_5_2_TSCCU1	0x54c
#define REG_5_2_LLPFOC1	0x554
#define REG_5_2_LLPCL1	0x558
#define REG_5_2_LLPCU1	0x55c
#define REG_5_2_SPBFCH	0x700
#define REG_5_2_SPBFCTL	0x704
#define REG_5_2_PPCH	0x800
#define REG_5_2_PPCTL	0x804
#define REG_5_2_IPPHC0LLPL	0x810
#define REG_5_2_IPPHC0LLPU	0x814
#define REG_5_2_IPPHC0LDPL	0x818
#define REG_5_2_IPPHC0LDPU	0x81c
#define REG_5_2_IPPHC1LLPL	0x820
#define REG_5_2_IPPHC1LLPU	0x824
#define REG_5_2_IPPHC1LDPL	0x828
#define REG_5_2_IPPHC1LDPU	0x82c
#define REG_5_2_IPPHC2LLPL	0x830
#define REG_5_2_IPPHC2LLPU	0x834
#define REG_5_2_IPPHC2LDPL	0x838
#define REG_5_2_IPPHC2LDPU	0x83c
#define REG_5_2_IPPHC3LLPL	0x840
#define REG_5_2_IPPHC3LLPU	0x844
#define REG_5_2_IPPHC3LDPL	0x848
#define REG_5_2_IPPHC3LDPU	0x84c
#define REG_5_2_IPPHC4LLPL	0x850
#define REG_5_2_IPPHC4LLPU	0x854
#define REG_5_2_IPPHC4LDPL	0x858
#define REG_5_2_IPPHC4LDPU	0x85c
#define REG_5_2_IPPHC5LLPL	0x860
#define REG_5_2_IPPHC5LLPU	0x864
#define REG_5_2_IPPHC5LDPL	0x868
#define REG_5_2_IPPHC5LDPU	0x86c
#define REG_5_2_IPPHC6LLPL	0x870
#define REG_5_2_IPPHC6LLPU	0x874
#define REG_5_2_IPPHC6LDPL	0x878
#define REG_5_2_IPPHC6LDPU	0x87c
#define REG_5_2_OPPHC0LLPL	0x880
#define REG_5_2_OPPHC0LLPU	0x884
#define REG_5_2_OPPHC0LDPL	0x888
#define REG_5_2_OPPHC0LDPU	0x88c
#define REG_5_2_OPPHC1LLPL	0x890
#define REG_5_2_OPPHC1LLPU	0x894
#define REG_5_2_OPPHC1LDPL	0x898
#define REG_5_2_OPPHC1LDPU	0x89c
#define REG_5_2_OPPHC2LLPL	0x8a0
#define REG_5_2_OPPHC2LLPU	0x8a4
#define REG_5_2_OPPHC2LDPL	0x8a8
#define REG_5_2_OPPHC2LDPU	0x8ac
#define REG_5_2_OPPHC3LLPL	0x8b0
#define REG_5_2_OPPHC3LLPU	0x8b4
#define REG_5_2_OPPHC3LDPL	0x8b8
#define REG_5_2_OPPHC3LDPU	0x8bc
#define REG_5_2_OPPHC4LLPL	0x8c0
#define REG_5_2_OPPHC4LLPU	0x8c4
#define REG_5_2_OPPHC4LDPL	0x8c8
#define REG_5_2_OPPHC4LDPU	0x8cc
#define REG_5_2_OPPHC5LLPL	0x8d0
#define REG_5_2_OPPHC5LLPU	0x8d4
#define REG_5_2_OPPHC5LDPL	0x8d8
#define REG_5_2_OPPHC5LDPU	0x8dc
#define REG_5_2_OPPHC6LLPL	0x8e0
#define REG_5_2_LOUTPAY0	0x8e0
#define REG_5_2_LINPAY0	0x8e2
#define REG_5_2_OPPHC6LLPU	0x8e4
#define REG_5_2_OPPHC6LDPL	0x8e8
#define REG_5_2_OPPHC6LDPU	0x8ec
#define REG_5_2_OPPHC7LLPL	0x8f0
#define REG_5_2_OPPHC7LLPU	0x8f4
#define REG_5_2_OPPHC7LDPL	0x8f8
#define REG_5_2_LOUTPAY1	0x8f8
#define REG_5_2_LINPAY1	0x8fa
#define REG_5_2_OPPHC7LDPU	0x8fc
#define REG_5_2_OPPHC8LLPL	0x900
#define REG_5_2_OPPHC8LLPU	0x904
#define REG_5_2_OPPHC8LDPL	0x908
#define REG_5_2_OPPHC8LDPU	0x90c
#define REG_5_2_IPPLC0CTL	0x910
#define REG_5_2_IPPLC0FMT	0x914
#define REG_5_2_IPPLC0LLPL	0x918
#define REG_5_2_IPPLC0LLPU	0x91c
#define REG_5_2_IPPLC1CTL	0x920
#define REG_5_2_IPPLC1FMT	0x924
#define REG_5_2_IPPLC1LLPL	0x928
#define REG_5_2_IPPLC1LLPU	0x92c
#define REG_5_2_IPPLC2CTL	0x930
#define REG_5_2_IPPLC2FMT	0x934
#define REG_5_2_IPPLC2LLPL	0x938
#define REG_5_2_IPPLC2LLPU	0x93c
#define REG_5_2_IPPLC3CTL	0x940
#define REG_5_2_IPPLC3FMT	0x944
#define REG_5_2_IPPLC3LLPL	0x948
#define REG_5_2_IPPLC3LLPU	0x94c
#define REG_5_2_IPPLC4CTL	0x950
#define REG_5_2_IPPLC4FMT	0x954
#define REG_5_2_IPPLC4LLPL	0x958
#define REG_5_2_IPPLC4LLPU	0x95c
#define REG_5_2_IPPLC5CTL	0x960
#define REG_5_2_IPPLC5FMT	0x964
#define REG_5_2_IPPLC5LLPL	0x968
#define REG_5_2_IPPLC5LLPU	0x96c
#define REG_5_2_IPPLC6CTL	0x970
#define REG_5_2_IPPLC6FMT	0x974
#define REG_5_2_IPPLC6LLPL	0x978
#define REG_5_2_IPPLC6LLPU	0x97c
#define REG_5_2_OPPLC0CTL	0x980
#define REG_5_2_OPPLC0FMT	0x984
#define REG_5_2_OPPLC0LLPL	0x988
#define REG_5_2_OPPLC0LLPU	0x98c
#define REG_5_2_OPPLC1CTL	0x990
#define REG_5_2_OPPLC1FMT	0x994
#define REG_5_2_OPPLC1LLPL	0x998
#define REG_5_2_OPPLC1LLPU	0x99c
#define REG_5_2_OPPLC2CTL	0x9a0
#define REG_5_2_OPPLC2FMT	0x9a4
#define REG_5_2_OPPLC2LLPL	0x9a8
#define REG_5_2_OPPLC2LLPU	0x9ac
#define REG_5_2_OPPLC3CTL	0x9b0
#define REG_5_2_OPPLC3FMT	0x9b4
#define REG_5_2_OPPLC3LLPL	0x9b8
#define REG_5_2_OPPLC3LLPU	0x9bc
#define REG_5_2_OPPLC4CTL	0x9c0
#define REG_5_2_OPPLC4FMT	0x9c4
#define REG_5_2_OPPLC4LLPL	0x9c8
#define REG_5_2_OPPLC4LLPU	0x9cc
#define REG_5_2_OPPLC5CTL	0x9d0
#define REG_5_2_OPPLC5FMT	0x9d4
#define REG_5_2_OPPLC5LLPL	0x9d8
#define REG_5_2_OPPLC5LLPU	0x9dc
#define REG_5_2_OPPLC6CTL	0x9e0
#define REG_5_2_OPPLC6FMT	0x9e4
#define REG_5_2_OPPLC6LLPL	0x9e8
#define REG_5_2_OPPLC6LLPU	0x9ec
#define REG_5_2_OPPLC7CTL	0x9f0
#define REG_5_2_OPPLC7FMT	0x9f4
#define REG_5_2_OPPLC7LLPL	0x9f8
#define REG_5_2_OPPLC7LLPU	0x9fc
#define REG_5_2_OPPLC8CTL	0xa00
#define REG_5_2_OPPLC8FMT	0xa04
#define REG_5_2_OPPLC8LLPL	0xa08
#define REG_5_2_OPPLC8LLPU	0xa0c
#define REG_5_2_MLCH	0xc00
#define REG_5_2_MLCD	0xc04
#define REG_5_2_LCAP0	0xc40
#define REG_5_2_LCTL0	0xc44
#define REG_5_2_LOSIDV0	0xc48
#define REG_5_2_LSDIID0	0xc4c
#define REG_5_2_LPSOO0	0xc50
#define REG_5_2_LPSIO0	0xc52
#define REG_5_2_LWALFC0	0xc58
#define REG_5_2_LCAP1	0xc80
#define REG_5_2_LCTL1	0xc84
#define REG_5_2_LOSIDV1	0xc88
#define REG_5_2_LSDIID1	0xc8c
#define REG_5_2_LPSOO1	0xc90
#define REG_5_2_LPSIO1	0xc92
#define REG_5_2_LWALFC1	0xc98
#define REG_5_2_IPPHC7LLPL	0x4a10
#define REG_5_2_IPPHC7LLPU	0x4a14
#define REG_5_2_IPPHC7LDPL	0x4a18
#define REG_5_2_IPPHC7LDPU	0x4a1c
#define REG_5_2_IPPHC8LLPL	0x4a20
#define REG_5_2_IPPHC8LLPU	0x4a24
#define REG_5_2_IPPHC8LDPL	0x4a28
#define REG_5_2_IPPHC8LDPU	0x4a2c
#define REG_5_2_IPPHC9LLPL	0x4a30
#define REG_5_2_IPPHC9LLPU	0x4a34
#define REG_5_2_IPPHC9LDPL	0x4a38
#define REG_5_2_IPPHC9LDPU	0x4a3c
#define REG_5_2_IPPHC10LLPL	0x4a40
#define REG_5_2_IPPHC10LLPU	0x4a44
#define REG_5_2_IPPHC10LDPL	0x4a48
#define REG_5_2_IPPHC10LDPU	0x4a4c
#define REG_5_2_IPPHC11LLPL	0x4a50
#define REG_5_2_IPPHC11LLPU	0x4a54
#define REG_5_2_IPPHC11LDPL	0x4a58
#define REG_5_2_IPPHC11LDPU	0x4a5c
#define REG_5_2_IPPHC12LLPL	0x4a60
#define REG_5_2_IPPHC12LLPU	0x4a64
#define REG_5_2_IPPHC12LDPL	0x4a68
#define REG_5_2_IPPHC12LDPU	0x4a6c
#define REG_5_2_IPPHC13LLPL	0x4a70
#define REG_5_2_IPPHC13LLPU	0x4a74
#define REG_5_2_IPPHC13LDPL	0x4a78
#define REG_5_2_IPPHC13LDPU	0x4a7c
#define REG_5_2_IPPHC14LLPL	0x4a80
#define REG_5_2_IPPHC14LLPU	0x4a84
#define REG_5_2_IPPHC14LDPL	0x4a88
#define REG_5_2_IPPHC14LDPU	0x4a8c
#define REG_5_2_OPPHC9LLPL	0x4a90
#define REG_5_2_OPPHC9LLPU	0x4a94
#define REG_5_2_OPPHC9LDPL	0x4a98
#define REG_5_2_OPPHC9LDPU	0x4a9c
#define REG_5_2_OPPHC10LLPL	0x4aa0
#define REG_5_2_OPPHC10LLPU	0x4aa4
#define REG_5_2_OPPHC10LDPL	0x4aa8
#define REG_5_2_OPPHC10LDPU	0x4aac
#define REG_5_2_OPPHC11LLPL	0x4ab0
#define REG_5_2_OPPHC11LLPU	0x4ab4
#define REG_5_2_OPPHC11LDPL	0x4ab8
#define REG_5_2_OPPHC11LDPU	0x4abc
#define REG_5_2_OPPHC12LLPL	0x4ac0
#define REG_5_2_OPPHC12LLPU	0x4ac4
#define REG_5_2_OPPHC12LDPL	0x4ac8
#define REG_5_2_OPPHC12LDPU	0x4acc
#define REG_5_2_OPPHC13LLPL	0x4ad0
#define REG_5_2_OPPHC13LLPU	0x4ad4
#define REG_5_2_OPPHC13LDPL	0x4ad8
#define REG_5_2_OPPHC13LDPU	0x4adc
#define REG_5_2_OPPHC14LLPL	0x4ae0
#define REG_5_2_OPPHC14LLPU	0x4ae4
#define REG_5_2_OPPHC14LDPL	0x4ae8
#define REG_5_2_OPPHC14LDPU	0x4aec
#define REG_5_2_IPPLC7CTL	0x4af0
#define REG_5_2_IPPLC7FMT	0x4af4
#define REG_5_2_IPPLC7LLPL	0x4af8
#define REG_5_2_IPPLC7LLPU	0x4afc
#define REG_5_2_IPPLC8CTL	0x4b00
#define REG_5_2_IPPLC8FMT	0x4b04
#define REG_5_2_IPPLC8LLPL	0x4b08
#define REG_5_2_IPPLC8LLPU	0x4b0c
#define REG_5_2_IPPLC9CTL	0x4b10
#define REG_5_2_IPPLC9FMT	0x4b14
#define REG_5_2_IPPLC9LLPL	0x4b18
#define REG_5_2_IPPLC9LLPU	0x4b1c
#define REG_5_2_IPPLC10CTL	0x4b20
#define REG_5_2_IPPLC10FMT	0x4b24
#define REG_5_2_IPPLC10LLPL	0x4b28
#define REG_5_2_IPPLC10LLPU	0x4b2c
#define REG_5_2_IPPLC11CTL	0x4b30
#define REG_5_2_IPPLC11FMT	0x4b34
#define REG_5_2_IPPLC11LLPL	0x4b38
#define REG_5_2_IPPLC11LLPU	0x4b3c
#define REG_5_2_IPPLC12CTL	0x4b40
#define REG_5_2_IPPLC12FMT	0x4b44
#define REG_5_2_IPPLC12LLPL	0x4b48
#define REG_5_2_IPPLC12LLPU	0x4b4c
#define REG_5_2_IPPLC13CTL	0x4b50
#define REG_5_2_IPPLC13FMT	0x4b54
#define REG_5_2_IPPLC13LLPL	0x4b58
#define REG_5_2_IPPLC13LLPU	0x4b5c
#define REG_5_2_IPPLC14CTL	0x4b60
#define REG_5_2_IPPLC14FMT	0x4b64
#define REG_5_2_IPPLC14LLPL	0x4b68
#define REG_5_2_IPPLC14LLPU	0x4b6c
#define REG_5_2_OPPLC9CTL	0x4b70
#define REG_5_2_OPPLC9FMT	0x4b74
#define REG_5_2_OPPLC9LLPL	0x4b78
#define REG_5_2_OPPLC9LLPU	0x4b7c
#define REG_5_2_OPPLC10CTL	0x4b80
#define REG_5_2_OPPLC10FMT	0x4b84
#define REG_5_2_OPPLC10LLPL	0x4b88
#define REG_5_2_OPPLC10LLPU	0x4b8c
#define REG_5_2_OPPLC11CTL	0x4b90
#define REG_5_2_OPPLC11FMT	0x4b94
#define REG_5_2_OPPLC11LLPL	0x4b98
#define REG_5_2_OPPLC11LLPU	0x4b9c
#define REG_5_2_OPPLC12CTL	0x4ba0
#define REG_5_2_OPPLC12FMT	0x4ba4
#define REG_5_2_OPPLC12LLPL	0x4ba8
#define REG_5_2_OPPLC12LLPU	0x4bac
#define REG_5_2_OPPLC13CTL	0x4bb0
#define REG_5_2_OPPLC13FMT	0x4bb4
#define REG_5_2_OPPLC13LLPL	0x4bb8
#define REG_5_2_OPPLC13LLPU	0x4bbc
#define REG_5_2_OPPLC14CTL	0x4bc0
#define REG_5_2_OPPLC14FMT	0x4bc4
#define REG_5_2_OPPLC14LLPL	0x4bc8
#define REG_5_2_OPPLC14LLPU	0x4bcc

#endif
