#include <common/register.h>

#ifndef CHAPTER_29_H
#define CHAPTER_29_H

const struct register_t summary_of_rtc_indexed_registers[14];
const struct register_t summary_of_rtc_pcr_registers[3];

#endif
