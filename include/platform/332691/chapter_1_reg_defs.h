#ifndef CHAPTER_1_REG_DEFS_H
#define CHAPTER_1_REG_DEFS_H

#define REG_1_1_ID	0x0
#define REG_1_1_CMD	0x4
#define REG_1_1_STS	0x6
#define REG_1_1_RID	0x8
#define REG_1_1_CC	0x9
#define REG_1_1_HTYPE	0xe
#define REG_1_1_SS	0x2c
#define REG_1_1_CAPP	0x34
#define REG_1_1_SCNT	0x64
#define REG_1_1_IOD	0x80
#define REG_1_1_IOE	0x82
#define REG_1_1_LGIR1	0x84
#define REG_1_1_LGIR2	0x88
#define REG_1_1_LGIR3	0x8c
#define REG_1_1_LGIR4	0x90
#define REG_1_1_ULKMC	0x94
#define REG_1_1_LGMR	0x98
#define REG_1_1_FS1	0xd0
#define REG_1_1_FS2	0xd4
#define REG_1_1_BDE	0xd8
#define REG_1_1_BC	0xdc
#define REG_1_1_PCCTL	0xe0

#define REG_1_2_GCFD	0x3418

#endif
