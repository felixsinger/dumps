#include <common/register.h>
#include <platform/332691/chapter_1.h>

const struct register_t summary_of_lpc_configuration_registers[] = {
	{ 0x0, 4, 1, 0x0, "ID", "Identifiers" },
	{ 0x4, 2, 0, 0x7, "CMD", "Device Command" },
	{ 0x6, 2, 0, 0x200, "STS", "Status" },
	{ 0x8, 1, 0, 0x0, "RID", "Revision ID" },
	{ 0x9, 4, 0, 0x60100, "CC", "Class Code" },
	{ 0xe, 1, 0, 0x80, "HTYPE", "Header Type" },
	{ 0x2c, 4, 0, 0x0, "SS", "Sub System Identifiers" },
	{ 0x34, 1, 0, 0x0, "CAPP", "Capability List Pointer" },
	{ 0x64, 1, 0, 0x10, "SCNT", "Serial IRQ Control" },
	{ 0x80, 2, 0, 0x0, "IOD", "I/O Decode Ranges" },
	{ 0x82, 2, 0, 0x0, "IOE", "I/O Enables" },
	{ 0x84, 4, 0, 0x0, "LGIR1", "LPC Generic I/O Range #1" },
	{ 0x88, 4, 0, 0x0, "LGIR2", "LPC Generic I/O Range #2" },
	{ 0x8c, 4, 0, 0x0, "LGIR3", "LPC Generic I/O Range #3" },
	{ 0x90, 4, 0, 0x0, "LGIR4", "LPC Generic I/O Range #4" },
	{ 0x94, 4, 0, 0x0, "ULKMC", "USB Legacy Keyboard/Mouse Control" },
	{ 0x98, 4, 0, 0x0, "LGMR", "LPC Generic Memory Range" },
	{ 0xd0, 4, 0, 0x112233, "FS1", "FWH ID Select #1" },
	{ 0xd4, 2, 0, 0x4567, "FS2", "FWH ID Select #2" },
	{ 0xd8, 2, 0, 0xffcf, "BDE", "BIOS Decode Enable" },
	{ 0xdc, 1, 0, 0x20, "BC", "BIOS Control" },
	{ 0xe0, 4, 0, 0x0, "PCCTL", "PCI Clock Control" },
};

const struct register_t summary_of_lpc_pcr_registers[] = {
	{ 0x3418, 4, 0, 0x0, "GCFD", "General Control & Function Disable" },
};
