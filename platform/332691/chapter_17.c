#include <common/register.h>
#include <platform/332691/chapter_17.h>

const struct register_t summary_of_keyboard_and_text_kt_pci_configuration_d22_f3_registers[] = {
	{ 0x0, 4, 1, 0x0, "KT_HOST_DID_VID", "Device ID and Vendor ID" },
	{ 0x4, 4, 0, 0xb00000, "KT_HOST_STS_CMD", "Status and Command" },
	{ 0x8, 4, 0, 0x7000200, "KT_HOST_CC_RID", "Class Code and Revision ID" },
	{ 0xc, 4, 0, 0x800000, "KT_HOST_BIST_HTYPE_LT_CLS", "BIST, Header Type, Latency Timer, and Cache Line Size" },
	{ 0x10, 4, 0, 0x1, "KT_HOST_IOBAR", "KT IO BAR" },
	{ 0x14, 4, 0, 0x0, "KT_HOST_MEMBAR", "KT Memory BAR" },
	{ 0x28, 4, 0, 0x0, "KT_HOST_CCP", "Cardbus CIS Pointer" },
	{ 0x2c, 4, 0, 0x8086, "KT_HOST_SID_SVID", "Subsystem ID and Subsystem Vendor ID" },
	{ 0x30, 4, 0, 0x0, "KT_HOST_XRBAR", "Expansion ROM Base Address" },
	{ 0x34, 4, 0, 0x40, "KT_HOST_CAPP", "Capabilities List Pointer" },
	{ 0x3c, 4, 0, 0x0, "KT_HOST_MAXL_MING_INTP_INTL", "Maximum Latency, Minimum Grant, Interrupt Pin and Interrupt Line" },
	{ 0x40, 4, 0, 0x805005, "KT_HOST_MSIMC_MSINP_MSICID", "MSI Message Control, Next Pointer and Capability ID" },
	{ 0x44, 4, 0, 0x0, "KT_HOST_MSIMA", "MSI Message Address" },
	{ 0x48, 4, 0, 0x0, "KT_HOST_MSIMUA", "MSI Message Upper Address" },
	{ 0x4c, 4, 0, 0x0, "KT_HOST_MSIMD", "MSI Message Data" },
	{ 0x50, 4, 0, 0x230001, "KT_HOST_PMCAP_PMNP_PMCID", "Power Management Capabilities, Next Pointer and Capability ID" },
	{ 0x54, 4, 0, 0x8, "KT_HOST_PMD_PMCSRBSE_PMCSR", "Power Management Data, Control/Status Register Bridge Support Extensions, Control and Status" },
};

const struct register_t summary_of_keyboard_and_text_kt_additional_configuration_registers[] = {
	{ 0x54, 4, 0, 0x8, "KT_CSXE_PMD_PMCSRBSE_PMCSR", "Power Management Control and Status" },
};
