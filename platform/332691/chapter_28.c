#include <common/register.h>
#include <platform/332691/chapter_28.h>

const struct register_t summary_of_interrupt_registers[] = {
	{ 0x20, 1, 0, 0x11, "MICW1", "Master Initialization Command Word 1" },
	{ 0x20, 1, 0, 0x0, "MOCW2", "Master Operational Control Word 2" },
	{ 0x20, 1, 0, 0x8, "MOCW3", "Master Operational Control Word 3" },
	{ 0x21, 1, 0, 0x0, "MICW2", "Master Initialization Command Word 2" },
	{ 0x21, 1, 0, 0x7, "MICW3", "Master Initialization Command Word 3" },
	{ 0x21, 1, 0, 0x0, "MICW4", "Master Initialization Command Word 4" },
	{ 0x21, 1, 0, 0x0, "MOCW1", "Master Operational Control Word 1" },
	{ 0xa0, 1, 0, 0x11, "SICW1", "Slave Initialization Command Word 1" },
	{ 0xa0, 1, 0, 0x0, "SOCW2", "Slave Operational Control Word 2" },
	{ 0xa0, 1, 0, 0x8, "SOCW3", "Slave Operational Control Word 3" },
	{ 0xa1, 1, 0, 0x0, "SICW2", "Slave Initialization Command Word 2" },
	{ 0xa1, 1, 0, 0x7, "SICW3", "Slave Initialization Command Word 3" },
	{ 0xa1, 1, 0, 0x0, "SICW4", "Slave Initialization Command Word 4" },
	{ 0xa1, 1, 0, 0x0, "SOCW1", "Slave Operational Control Word 1" },
	{ 0x4d0, 1, 0, 0x0, "ELCR1", "Master Edge/Level Control" },
	{ 0x4d1, 1, 0, 0x0, "ELCR2", "Slave Edge/Level Control" },
};

const struct register_t summary_of_interrupt_pcr_registers[] = {
	{ 0x3100, 1, 0, 0x80, "PARC", "PIRQA Routing Control" },
	{ 0x3101, 1, 0, 0x80, "PBRC", "PIRQB Routing Control" },
	{ 0x3102, 1, 0, 0x80, "PCRC", "PIRQC Routing Control" },
	{ 0x3103, 1, 0, 0x80, "PDRC", "PIRQD Routing Control" },
	{ 0x3104, 1, 0, 0x80, "PERC", "PIRQE Routing Control" },
	{ 0x3105, 1, 0, 0x80, "PFRC", "PIRQF Routing Control" },
	{ 0x3106, 1, 0, 0x80, "PGRC", "PIRQG Routing Control" },
	{ 0x3107, 1, 0, 0x80, "PHRC", "PIRQH Routing Control" },
	{ 0x3140, 2, 0, 0x3210, "PIR0", "PCI Interrupt Route 0" },
	{ 0x3142, 2, 0, 0x0, "PIR1", "PCI Interrupt Route 1" },
	{ 0x3144, 2, 0, 0x0, "PIR2", "PCI Interrupt Route 2" },
	{ 0x3146, 2, 0, 0x0, "PIR3", "PCI Interrupt Route 3" },
	{ 0x3148, 2, 0, 0x0, "PIR4", "PCI Interrupt Route 4" },
	{ 0x31fc, 4, 0, 0x0, "GIC", "General Interrupt Control" },
	{ 0x3200, 4, 0, 0xff0000, "IPC0", "Interrupt Polarity Control 0" },
	{ 0x3204, 4, 0, 0x0, "IPC1", "Interrupt Polarity Control 1" },
	{ 0x3208, 4, 0, 0x0, "IPC2", "Interrupt Polarity Control 2" },
	{ 0x320c, 4, 0, 0x0, "IPC3", "Interrupt Polarity Control 3" },
	{ 0x3300, 4, 0, 0x0, "ITSSPRC", "ITSS Power Reduction Control" },
	{ 0x3334, 2, 0, 0x0, "MMC", "Master Message Control" },
};
