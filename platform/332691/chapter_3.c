#include <common/register.h>
#include <platform/332691/chapter_3.h>

const struct register_t summary_of_p2sb_configuration_registers[] = {
	{ 0x0, 4, 0, 0xc5c58086, "PCIID", "PCI Identifier" },
	{ 0x4, 2, 0, 0x4, "PCICMD", "PCI Command" },
	{ 0x8, 1, 0, 0x0, "PCIRID", "Revision ID" },
	{ 0x9, 4, 0, 0x58000, "PCICC", "Class Code" },
	{ 0xe, 1, 0, 0x0, "PCIHTYPE", "PCI Header Type" },
	{ 0x10, 4, 0, 0x4, "SBREG_BAR", "Sideband Register Access BAR" },
	{ 0x14, 4, 0, 0x0, "SBREG_BARH", "Sideband Register BAR High DWORD" },
	{ 0x2c, 4, 0, 0x0, "PCIHSS", "PCI Subsystem Identifiers" },
	{ 0x50, 2, 0, 0xf8, "VBDF", "VLW Bus:Device:Function" },
	{ 0x52, 2, 0, 0xf8, "EBDF", "ERROR Bus:Device:Function" },
	{ 0x54, 4, 0, 0xc700, "RCFG", "Routing Configuration" },
	{ 0x60, 1, 0, 0x0, "HPTC", "High Performance Event Timer Configuration" },
	{ 0x64, 2, 0, 0x0, "IOAC", "IOxAPIC Configuration" },
	{ 0x6c, 2, 0, 0xf8, "IBDF", "IOxAPIC Bus:Device:Function" },
	{ 0x70, 2, 0, 0xf8, "HBDF", "HPET Bus:Device:Function" },
	{ 0x80, 4, 0, 0x0, "SBREGPOSTED0", "Sideband Register posted 0" },
	{ 0x84, 4, 0, 0x0, "SBREGPOSTED1", "Sideband Register posted 1" },
	{ 0x88, 4, 0, 0x0, "SBREGPOSTED2", "Sideband Register posted 2" },
	{ 0x8c, 4, 0, 0x0, "SBREGPOSTED3", "Sideband Register posted 3" },
	{ 0x90, 4, 0, 0x0, "SBREGPOSTED4", "Sideband Register posted 4" },
	{ 0x94, 4, 0, 0x0, "SBREGPOSTED5", "Sideband Register posted 5" },
	{ 0x98, 4, 0, 0x0, "SBREGPOSTED6", "Sideband Register posted 6" },
	{ 0x9c, 4, 0, 0x0, "SBREGPOSTED7", "Sideband Register posted 7" },
	{ 0xa0, 4, 0, 0x60010, "DISPBDF", "Display Bus:Device:Function" },
	{ 0xa4, 2, 0, 0x0, "ICCOS", "ICC Register Offsets" },
	{ 0xb0, 4, 0, 0x0, "EPMASK0", "Endpoint Mask 0" },
	{ 0xb4, 4, 0, 0x0, "EPMASK1", "Endpoint Mask 1" },
	{ 0xb8, 4, 0, 0x0, "EPMASK2", "Endpoint Mask 2" },
	{ 0xbc, 4, 0, 0x0, "EPMASK3", "Endpoint Mask 3" },
	{ 0xc0, 4, 0, 0x0, "EPMASK4", "Endpoint Mask 4" },
	{ 0xc4, 4, 0, 0x0, "EPMASK5", "Endpoint Mask 5" },
	{ 0xc8, 4, 0, 0x0, "EPMASK6", "Endpoint Mask 6" },
	{ 0xcc, 4, 0, 0x0, "EPMASK7", "Endpoint Mask 7" },
	{ 0xd0, 4, 0, 0x0, "SBIADDR", "SBI Address" },
	{ 0xd4, 4, 0, 0x0, "SBIDATA", "SBI Data" },
	{ 0xd8, 2, 0, 0x0, "SBISTAT", "SBI Status" },
	{ 0xda, 2, 0, 0x0, "SBIRID", "SBI Routing Identification" },
	{ 0xdc, 4, 0, 0x0, "SBIEXTADDR", "SBI Extended Address" },
	{ 0xe0, 4, 0, 0x0, "P2SBC", "P2SB Control" },
	{ 0xe4, 1, 0, 0x1, "PCE", "Power Control Enable" },
};
