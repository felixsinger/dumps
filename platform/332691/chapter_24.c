#include <common/register.h>
#include <platform/332691/chapter_24.h>

const struct register_t summary_of_processor_interface_memory_registers[] = {
	{ 0x61, 1, 0, 0x0, "NMI_STS_CNT", "NMI Status and Control" },
	{ 0x70, 1, 0, 0x80, "NMI_EN", "NMI Enable (and Real Time Clock Index)" },
	{ 0x92, 1, 0, 0x0, "PORT92", "Init Register" },
	{ 0xcf9, 1, 0, 0x0, "RST_CNT", "Reset Control Register" },
};
