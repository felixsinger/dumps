#include <common/register.h>
#include <platform/332691/chapter_2.h>

const struct register_t summary_of_enhanced_spi_espi_pci_configuration_registers[] = {
	{ 0x0, 4, 1, 0x0, "ESPI_DID_VID", "Identifiers" },
	{ 0x4, 4, 0, 0x403, "ESPI_STS_CMD", "Device Status and Command" },
	{ 0x8, 4, 0, 0x6010000, "ESPI_CC_RID", "Class Code and Revision ID" },
	{ 0xc, 4, 0, 0x800000, "ESPI_BIST_HTYPE_PLT_CLS", "BIST, Header Type, Primary Latency Timer, Cache Line Size" },
	{ 0x2c, 4, 0, 0x0, "ESPI_SS", "Sub System Identifiers" },
	{ 0x34, 4, 0, 0xe0, "ESPI_CAPP", "Capability List Pointer" },
	{ 0x80, 4, 0, 0x0, "ESPI_IOD_IOE", "I/O Decode Ranges and I/O Enables" },
	{ 0x84, 4, 0, 0x0, "ESPI_LGIR1", "eSPI Generic I/O Range #1" },
	{ 0x88, 4, 0, 0x0, "ESPI_LGIR2", "eSPI Generic I/O Range #2" },
	{ 0x8c, 4, 0, 0x0, "ESPI_LGIR3", "eSPI Generic I/O Range #3" },
	{ 0x90, 4, 0, 0x0, "ESPI_LGIR4", "eSPI Generic I/O Range #4" },
	{ 0x94, 4, 0, 0x0, "ESPI_ULKMC", "USB Legacy Keyboard/Mouse Control" },
	{ 0x98, 4, 0, 0x0, "ESPI_LGMR", "eSPI Generic Memory Range" },
	{ 0xd0, 4, 0, 0x112233, "ESPI_FS1", "FWH ID Select #1" },
	{ 0xd4, 4, 0, 0x4567, "ESPI_FS2", "FWH ID Select #2" },
	{ 0xd8, 4, 0, 0xffcf, "ESPI_BDE", "BIOS Decode Enable" },
	{ 0xdc, 4, 0, 0x20, "ESPI_BC", "BIOS Control" },
};

const struct register_t summary_of_espi_pcr_registers[] = {
	{ 0x4000, 4, 0, 0x0, "SLV_CFG_REG_CTL", "eSPI Slave Configuration and Link Control" },
	{ 0x4004, 4, 0, 0x0, "SLV_CFG_REG_DATA", "eSPI Slave Configuration Register Data" },
	{ 0x4020, 4, 0, 0x0, "PCERR_SLV0", "Peripheral Channel Error for Slave 0" },
	{ 0x4030, 4, 0, 0x0, "VWERR_SLV0", "Virtual Wire Channel Error for Channel 0" },
	{ 0x4040, 4, 0, 0x0, "FCERR_SLV0", "Flash Access Channel Error for Slave 0" },
	{ 0x4050, 4, 0, 0xff00, "LNKERR_SL0", "Link Error for Slave 0" },
};
