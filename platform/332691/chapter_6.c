#include <common/register.h>
#include <platform/332691/chapter_6.h>

const struct register_t summary_of_smbus_configuration_registers[] = {
	{ 0x0, 2, 0, 0x8086, "VID", "Vendor ID" },
	{ 0x2, 2, 1, 0x0, "DID", "Device ID" },
	{ 0x4, 2, 0, 0x0, "CMD", "Command" },
	{ 0x6, 2, 0, 0x280, "DS", "Device Status" },
	{ 0x8, 1, 0, 0x0, "RID", "Revision ID" },
	{ 0x9, 1, 0, 0x0, "PI", "Programming Interface" },
	{ 0xa, 1, 0, 0x5, "SCC", "Sub Class Code" },
	{ 0xb, 1, 0, 0xc, "BCC", "Base Class Code" },
	{ 0x10, 4, 0, 0x4, "SMBMBAR_31_0", "SMBus Memory Base Address_31_0" },
	{ 0x14, 4, 0, 0x0, "SMBMBAR_63_32", "SMBus Memory Base Address_63_32" },
	{ 0x20, 4, 0, 0x1, "SBA", "SMB Base Address" },
	{ 0x2c, 2, 0, 0x0, "SVID", "SVID" },
	{ 0x2e, 2, 0, 0x0, "SID", "SID" },
	{ 0x3c, 1, 0, 0x0, "INTLN", "Interrupt Line" },
	{ 0x3d, 1, 0, 0x1, "INTPN", "Interrupt Pin" },
	{ 0x40, 1, 0, 0x0, "HCFG", "Host Configuration" },
	{ 0x50, 4, 0, 0x1, "TCOBASE", "TCO Base Address" },
	{ 0x54, 4, 0, 0x0, "TCOCTL", "TCO Control" },
	{ 0x64, 4, 0, 0x0, "HTIM", "Host SMBus Timing" },
	{ 0x80, 4, 0, 0x40000, "SMBSM", "SMBus Power Gating" },
};

const struct register_t summary_of_smbus_i_o_and_memory_mapped_i_o_registers[] = {
	{ 0x0, 1, 0, 0x0, "HSTS", "Host Status Register Address" },
	{ 0x2, 1, 0, 0x0, "HCTL", "Host Control Register" },
	{ 0x3, 1, 0, 0x0, "HCMD", "Host Command Register" },
	{ 0x4, 1, 0, 0x0, "TSA", "Transmit Slave Address Register" },
	{ 0x5, 1, 0, 0x0, "HD0", "Data 0 Register" },
	{ 0x6, 1, 0, 0x0, "HD1", "Data 1 Register" },
	{ 0x7, 1, 0, 0x0, "HBD", "Host Block Data" },
	{ 0x8, 1, 0, 0x0, "PEC", "Packet Error Check Data Register" },
	{ 0x9, 1, 0, 0x44, "RSA", "Receive Slave Address Register" },
	{ 0xa, 2, 0, 0x0, "SD", "Slave Data Register" },
	{ 0xc, 1, 0, 0x0, "AUXS", "Auxiliary Status" },
	{ 0xd, 1, 0, 0x0, "AUXC", "Auxiliary Control" },
	{ 0xe, 1, 0, 0x4, "SMLC", "SMLINK_PIN_CTL Register" },
	{ 0xf, 1, 0, 0x4, "SMBC", "SMBUS_PIN_CTL Register" },
	{ 0x10, 1, 0, 0x0, "SSTS", "Slave Status Register" },
	{ 0x11, 1, 0, 0x0, "SCMD", "Slave Command Register" },
	{ 0x14, 1, 0, 0x0, "NDA", "Notify Device Address Register" },
	{ 0x16, 1, 0, 0x0, "NDLB", "Notify Data Low Byte Register" },
	{ 0x17, 1, 0, 0x0, "NDHB", "Notify Data High Byte Register" },
};

const struct register_t summary_of_smbus_pcr_registers[] = {
	{ 0x0, 4, 0, 0x0, "TCOCFG", "TCO Configuration" },
	{ 0xc, 4, 0, 0x0, "GC", "General Control" },
	{ 0x10, 4, 0, 0x9, "PCE", "Power Control Enable" },
};
